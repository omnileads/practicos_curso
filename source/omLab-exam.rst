****************************
OMniLeads Certification Test
****************************

Bienvenido al examen de certificación de OMniLeads "The Open Source Contact Center Solution". 

Usted debe tener a disposición un email con material necesario para utilizar durante el examen, esto es:

* Una dirección IP pública donde debe registrar tanto el SIP trunk de OMniLeads como la cuenta SIP utilizada como Teléfono de la red pública.
* Una base de contactos
* Un audio de Música de espera, un audio para IVR y un audio para el módulo de identificación de clientes

El método utilizado implementa:

* Un host sobre el que se debe instalar OMniLeads
* Un componente PSTN-Emulator que servirá para simular llamadas salientes y entrantes con su instancia de OMniLeads
* Un softphone (o hardphone si así lo desea) capaz de registrar una cuenta SIP en el PSTN-Emulator para utilizar como terminal de suscriptor

.. image:: ../img/omnileads-cert-test.png
*Arquitectura del examen*

A continuación se desplegarán una serie de ítems que deberá resolver para aprobar el examen y obtener la certificación.

Instalación de OMniLeads
************************

A continuación deberá ejecutar una instalación "Self-Hosted" de la aplicación. Para acceder vía SSH deberá utilizar los datos proporcionados (dirección IP y clave pública SSH).

Parámetros para la instalación:

.. code-block:: bash 

 hostname: test.omnileads.net
 admin password: 765765AAA
 mysql password: 765765AAA
 postgres password: 765765AAA

El resto de los parámetros puede usar los valores por defecto.


Preparación de la cuenta SIP (subscriber)
*****************************************
En esta sección deberá dejar listo la cuenta SIP (Subscriber) que se utilizará a lo largo del examen para emular la interacción telefónica con la PSTN. 
Con esta cuenta configurada en un Softphone (recomendamos Zoiper), podrá simular llamadas entrantes en OMniLeads así como también llamadas salientes.

* Registrar un SIP phone suscriptor (opcionalmente puede registrar un segundo SIP phone)

.. code-block:: bash

 SIP username 1: 72007200
 SIP username 2: 22002200
 SIP password: omnileads
 SIP Server: IP = 54.193.12.247 & port = 7272
 Codecs: g711A, g711U

* Probar lanzar una llamada a cualquier número telefónico.

Configuración inicial de OMniLeads
**********************************

* Crear un usuario supervisor y dos usuarios tipo agente
* Subir la base de contactos proporcionada
* Subir los dos audios proporcionados
* Subir el archivo de Música de Espera
* Generar 4 calificaciones (busy, no answer, wrong number y success) y 3 pausas (coaching, lunch, break)
* Crear dos pausas (coaching & lunch)
* Generar un formulario web con el siguiente aspecto 

 .. image:: ../img/campaigns_newform.png

 Donde los campos tipo lista deberán contener los valores del 1 al 5.

Configuración de acceso a la PSTN
*********************************

Considere el siguiente tipo de arquitectura de acceso a la red SIP del proveedor:

.. image:: ../img/sip-trunk-oml-pstn.png

* Dado los datos del proveedor SIP, configure el troncal en la plataforma.

.. code-block:: bash

 SIP username: 40404040
 SIP password: omnileads
 Codecs: g711A, g711U
 SIP Server: Este dato se proporciona en el email de bienvenida

Este troncal implica realizar una registración SIP y además autenticarse en las llamadas salientes.

* Generar una ruta saliente que permita enviar llamadas por el troncal generado en el paso anterior.


Configuración de campaña Preview
********************************

* Generar una campaña Preview que utilice la base de contactos previamente cargada, el formulario de encuesta creado anteriormente, las calificaciones también generadas, utilizando  la calificación *success* como calificación de gestión para disparar el formulario de campaña.
* Loguearse como agente y generar 5 llamadas con diferentes calificaciones.

Configuración de llamadas entrantes
***********************************

En este ítem vamos trabajar con el procesamiento de las llamadas entrantes. Para aprobar el punto deberíamos conseguir que nuestro OMniLeads implemente el siguiente esquema:


.. image:: ../img/lab-incalls-arq.png
*Figura: esquema implementado*


* Crear dos campañas entrantes (campaña_A y campaña_B) con un agente asignado a cada una. En la campaña_A configure la MOH subida en los pasos anteriores.
* Generar una ruta entrante para el *DID: 40404040* cuyo destino sea nuestra campaña_A.
* Crear un IVR utilizando nuestro *audio* facilitado. El menú deberá presentar para el *DTMF 1* que la llamada se encamine hacia la Campaña A, y para el *DTMF 2* hacia la Campaña B.
* Generar una nueva ruta entrante para el *DID 40404041* y encaminar la llamada hacia el recientemente creado IVR.
* Generar un grupo horario *lun-vie 09:00 a 18:00*.
* Crear un condicional de tiempo asociado al grupo horario anterior, que en caso de coincidir dentro del rango horario del grupo la llamada se encamina
hacia nuestro IVR mientras que en caso de caer fuera del rango horario del grupo, la llamada se envíe hacia un Hangup.
* Generar una nueva ruta entrante para el *DID 40404042* y encaminar la llamada hacia el recientemente Condicional de Tiempo creado.
* En este ítem vamos a generar un destino personalizado.

.. image:: ../img/lab-incalls-custom-dst.png

Una vez generado el *custom destionation* en OMniLeads, procedemos a generar el dialplan en el archivo pertinente para ello, es decir: **oml_extensions_custom.conf**

.. code-block:: bash

 [oml_course]
 exten => s,1,Verbose(2, custom dst oml course)
 same => n,Playback(tt-weasels)
 same => n,Hangup()

Luego hacer un "dialplan reload" en el CLI de Asterisk.

Finalmente, podemos añadir un nuevo *DTMF* dentro del IVR existente que invoque al destino personalizado.


Configuración de campañas con discador predictivo
*************************************************

En esta sección se debe dejar funcionando una campaña con discador predictivo enviando llamadas hacia nuestro banco de pruebas. 

* Configurar el módulo de discador predictivo Wombat Dialer.
* Crear una campaña con la base de contactos. 
* Loguear un agente y procesar las llamadas de la campaña.
* Al finalizar la campaña, reciclar los registros no contactados de la base de datos, sobre una nueva campaña predictiva.

Backup & Actualizaciones
************************

* Generar un backup de la instancia.
* Aplicar una actualización hacia la rama "develop".






