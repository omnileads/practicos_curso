.. _omLab-pstn:

*********************************
Configuración de salida a la PSTN
*********************************

En esta sección vamos a configurar un troncal SIP entre la instancia de OMniLeads y el banco de pruebas o *PSTN Emulator*.

La siguiente sección de la `Documentación Oficial <https://documentacion-omnileads.readthedocs.io/es/develop/telephony_pjsip_templates.html#about-telephony-pjsip-lan-provider>`_
cubre todos estos aspectos.

Crear PJSIP Trunk en OMniLeads
******************************

En este punto se debe generar un nuevo SIP Trunk utilizando PJSIP como módulo subyacente. Para el caso de este práctico aplica la plantilla *Custom*, ya que
OMniLeads y el host PSTN-Emulator se encuentran dentro del mismo segmento de red (por ello el parámetro *transport=trunk-transport*), pero a diferencia de un escenario de proveedor con "backbone privado", aquí
vamos a aplicar un registro de OMniLeads hacia el PSTN-Emulator (por ello el parámetro *sends_registrations=yes*)

Por lo tanto vamos a ingresar el siguiente bloque de configuración:

.. code-block:: bash

 type=wizard
 transport=trunk-transport
 accepts_registrations=no
 accepts_auth=no
 sends_registrations=yes
 sends_auth=yes
 endpoint/rtp_symmetric=yes
 endpoint/force_rport=yes
 endpoint/rewrite_contact=yes
 endpoint/timers=yes
 aor/qualify_frequency=60
 endpoint/allow=alaw,ulaw
 endpoint/dtmf_mode=rfc4733
 endpoint/context=from-pstn
 remote_hosts=XXX.XXX.XXX.XXX:5060
 endpoint/from_user=40404040
 outbound_auth/username=40404040
 outbound_auth/password=omnileads

Donde XXX.XXX.XXX.XXX es la IP del pstn-emulator.

Comprobar disponibilidad del trunk y registro
*********************************************

Para comprobar si el Asterisk de OMniLeads llega a ver disponible el trunk del otro extremo se debe ejecutar el comando:

.. code-block:: bash

  asterisk -rx 'pjsip show endpoints'

En caso positivo, se debería visualizar un status de *Avail*.

.. image:: ../img/pjsip-show-endpoints.png


Para comprobar si el Asterisk de OMniLeads se encuentra correctamente registrado en el proveedor SIP, lanzar el comando:

.. code-block:: bash

  asterisk -rx 'pjsip show registrations'

En caso positivo, se debería visualizar un status de *Registered*.

.. image:: ../img/pjsip-show-registrations.png

Generar ruta saliente
*********************

Finalmente se procede con la generación de una ruta saliente cuyo troncal de salida sea el trunk creado recientemente.

.. image:: ../img/out-route.png
