*******************
Integración con CRM
*******************

En esta práctica vamos a explorar la integración entre OMniLeads y un sistema de gestión externo.

Las siguientes secciones de la Documentación Oficial cubren todos estos aspectos:

`Integración con CRM <https://documentacion-omnileads.readthedocs.io/es/develop/crm_integration.html>`_

`RESTful API <https://documentacion-omnileads.readthedocs.io/es/develop/api.html>`_


OML2CRM
*******

Generar una interacción desde OMniLeads hacia una URL (servicio web o CRM) en cada llamada conectada entre un Agent y un Número externo (cliente / abonado).

**Activar un nuevo sistema externo**

Crear un punto de interacción CRM utilizando la URL: crm-oml2crm-nuevo-sitio-externo.png y los parámetros de la figura

.. image:: ../img/crm-oml2crm-nuevo-sitio-externo.png

**Configurar una campaña con interacción al sistema externo**

Ahora vamos a crear una campaña Preview utilizando el recurso de disparar una URL específica con parámetros de una llamada/campaña opcionalmente.
Para ello generamos una campaña Preview y como primer punto seleccionamos el Sitio Externo con el cual vamos a trabajar en la campaña.

.. image:: ../img/crm-oml2crm-camp-1.png

Finalmente poner atención en los parámetros que deseamos enviar, podemos implementar tal cual la figura:

.. image:: ../img/crm-oml2crm-camp-2.png

Tengamos en cuenta que se debe respetar tal cual el nombre de cada columna de la base de contactos, cuando se requiera invocar una columna
de la misma a la hora de generar la llamada al URL.

.. image:: ../img/crm-oml2crm-camp-3.png

**Probar en las llamas**

Ahora utilice cualquier agente para disparar llamadas preview desde nuestra nueva campaña.

.. image:: ../img/crm-oml2crm-nuevo-sitio-externo.png
