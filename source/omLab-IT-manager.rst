**********************
Gestiones del sysadmin
**********************

En los próximos pasos de este laboratorio vamos a estar trabajando con gestiones tíícas que debe llevar a cabo el sysadmin de OMniLeads.

La siguiente sección de la `Documentación Oficial <https://documentacion-omnileads.readthedocs.io/es/develop/maintance.html>`_ cubre todos estos aspectos.

Backup/Restore
**************

Para este punto precisamos de una instancia de OMniLeads virgen sobre la cual vamos a llevar a cabo el restore. La instancia deberá tener la misma versión,
misma IP/hostname y mismas contraseñas.

Sobre la instancia que estuvimos trabajando a lo largo de todo el curso, tomar un backup:

.. code-block:: bash

 su omnileads -
 cd /opt/omnileads/bin
 ./backup-restore.sh -b

El backup se genera en el directorio /opt/omnileads/backup.

Se deberá mover dicho archivo hacia la instancia donde se desea aplicar el restore (en el directorio /opt/omnileads/backup)

Ingresar vía SSH a la instancia de Restore y ejecutar los comandos:

.. code-block:: bash

 su omnileads -
 cd /opt/omnileads/bin
 ./backup-restore.sh -r nombre_del_archivo_de_backup


Actualizaciones
***************

Para realizar una actualización, nos vamos a parar sobre el repositorio. Nos vamos a cambiar a la rama "develop" y allí ejecutar un post-install.

.. code-block:: bash

 cd ominicontacto
 git checkout develop
 git pull origin develop

Una vez parados sobre la rama a implementar, debemos editar el archivo *inventory*.

Si estamos en modo "deployer & nodes"

.. code-block:: bash

 ##########################################################################################
 # If you are installing a prodenv (PE) AIO y bare-metal, change the IP and hostname here #
 ##########################################################################################
 [prodenv-aio]
 #localhost ansible_connection=local ansible_user=root #(this line is for self-hosted installation)
 192.168.95.100 ansible_ssh_port=22 ansible_user=root #(this line is for node-host installation)


Es decir debemos editar la IP de nuestra instancia y luego ejecutar el script de deploy.

.. code-block:: bash

 sudo ./deploy.sh -u

Si estamos en modo "self-hosted"

.. code-block:: bash

 ##########################################################################################
 # If you are installing a prodenv (PE) AIO y bare-metal, change the IP and hostname here #
 ##########################################################################################
 [prodenv-aio]
 localhost ansible_connection=local ansible_user=root #(this line is for self-hosted installation)
 #XXX.XXX.XXX.XXX ansible_ssh_port=22 ansible_user=root #(this line is for node-host installation)

Es decir debemos descomentar la linea correspondiente y luego ejecutar el script de deploy.

.. code-block:: bash

 ./deploy.sh -u --iface=NIC_NAME


 Cambios de contraseñas
 **********************

 Para llevar adelante modificaciones en las contraseñas:

 * admin
 * postgres
 * asterisk AMI
 * wombat dialer
 * mysql

Debemos hacer un post-install habiendo antes modificado las contraseñas deseadas en el archivo de *inventory*.

Para este práctico, modificar la contraseña de *admin* y volver a ingresar al sistema utilizando dicha contraseña modificada.

Cambios de IP, hostname
***********************

Para ello se debe cambiar los parámetros de red deseados en el server que aloja la instancia de OMniLeads,reiniciar para que tome los valores.
Luego se ejecuta un post-install.

Si estamos en modo "deployer & nodes"

.. code-block:: bash

 ##########################################################################################
 # If you are installing a prodenv (PE) AIO y bare-metal, change the IP and hostname here #
 ##########################################################################################
 [prodenv-aio]
 #localhost ansible_connection=local ansible_user=root #(this line is for self-hosted installation)
 192.168.95.100 ansible_ssh_port=22 ansible_user=root #(this line is for node-host installation)

Es decir debemos descomentar e indicar la NUEVA IP de nuestra instancia y luego ejecutar el script de deploy.

.. code-block:: bash

 sudo ./deploy.sh -u

Si estamos en modo "self-hosted"

.. code-block:: bash

 ##########################################################################################
 # If you are installing a prodenv (PE) AIO y bare-metal, change the IP and hostname here #
 ##########################################################################################
 [prodenv-aio]
 localhost ansible_connection=local ansible_user=root #(this line is for self-hosted installation)
 #XXX.XXX.XXX.XXX ansible_ssh_port=22 ansible_user=root #(this line is for node-host installation)

Es decir debemos descomentar la linea correspondiente y luego ejecutar el script de deploy.

.. code-block:: bash

 ./deploy.sh -u --iface=NIC_NAME


En este punto, modificar la IP de la instancia de OMniLeads.
