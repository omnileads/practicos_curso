*****************
Campañas manuales
*****************

La siguiente sección de la `Documentación Oficial <https://documentacion-omnileads.readthedocs.io/es/develop/campaigns_manual.html#about-manualcamp>`_ cubre todos estos aspectos.

**Crear una campaña manual**

No hace falta asociar una base de contactos, podemos asignar el formulario "encuesta" y el lote de calificaciones.
La calificación "Encuesta realizada" deberá desplegar el formulario en cuestión.

**Ejecutar una serie de llamadas y probar diferentes calificaciones**

Marque algunas llamadas a números arbitrarios utilizando como salida la campaña manual generada en el paso anterior.

Luego puede probar generar una llamada manual a un número, guardalos como contacto y calificar el mismo.
