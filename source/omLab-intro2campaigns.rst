.. _omLab-intreo2campaigns:

***********************************
Configuraciones comunes de campañas
***********************************

En esta práctica vamos a generar alguno de los elementos comunes que toda campaña precisa relacionar para poder existir operativamente.


La siguiente sección de la `Documentación Oficial <https://documentacion-omnileads.readthedocs.io/es/develop/campaigns.html#>`_ cubre todos estos aspectos.

Bases de contactos
******************

Subir al sistema las dos bases de contactos facilitadas.

Formularios
***********

Generar dos formularios de campaña.

El primer formulario deberá implementar una encuesta similar a la siguiente:

.. image:: ../img/lab-comunes-camps-form11.png
*Figura: formulario de campaña A*

El segundo formulario "venta" tendrá el siguiente aspecto:

.. image:: ../img/lab-comunes-camps-form22.png
*Figura: formulario de campaña B*

Calificaciones
**************

Generar las sigueintes calificaciones de agente:

* Ocupado
* No contesta
* Telefono erroneo
* Abonado fuera de servicio
* Equivocado
* Turno asignado
* Encuesta realizada
