.. _omLab-basic-configuration:

*********************
Configuración Inicial
*********************

En este laboratorio vamos a configurar asuntos escenciales como:

* Agentes y Grupos de Agentes
* Supervisores
* Bitácora de audios personales
* Música de espera
* Pausas de Agentes
* Registro de la instancia

La siguiente sección de la `Documentación Oficial <https://documentacion-omnileads.readthedocs.io/es/develop/initial_settings.html#/>`_ cubre todos estos aspectos.

Crear usuarios y grupos
***********************

En este punto debemos crear:

* Un grupo de agentes (con las opciones de auto atención dialer y autoatención inbound activadas)
* Dos agentes dentro del mismo
* Un supervisor del tipo *supervisor administrador*.


Audios y música de espera
**************************

Subir el audio facilitado con el material del curso.

Crear una Playlist de MOH, utilizando como audio el archivo de MOH facilitado.

Pausas
******

Crear las pausas: coaching (productiva) y break (recreativa).

Registro de la instancia
************************

Proceder con el registro de la instanacia de OMniLeads.
