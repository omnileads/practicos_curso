*******************
Integración con PBX
*******************

En este tramo del curso vamos a trabajar con la instancia *OMniLeads inside PBX*, que dejamos instalada en el práctico de deploys.
Por lo tanto debemos encender la instancia y manos a la obra.

La siguiente sección de la `Documentación Oficial <https://documentacion-omnileads.readthedocs.io/es/develop/pbx_integration.html>`_ cubre todos estos aspectos.

Troncalizar ambos sistemas
**************************

**Troncal en OMniLeads**

Utilizando la plantilla PBX LAN, vamos a considerar los últimos 6 parámetros:

.. code-block:: bash

 inbound_auth/username=fpbx
 remote_hosts=XXX.XXX.XXX.PBX:5060
 inbound_auth/password=omlpbx
 outbound_auth/username=omnileads
 outbound_auth/password=omlpbx
 endpoint/from_user=omnileads


Dónde **XXX.XXX.XXX.PBX** es la dirección IP de FreePBX

**Troncal en FreePBX**

Nos ajustamos a los valores que presentan la figura:

 .. image:: ../img/pbx-trunk-pbx2oml.png


 .. image:: ../img/pbx-trunk-pbx2oml-2.png

Los parámetros indicados en las figuras deben ser considentes con los de su configuración.

Enrutamiento de llamadas entra ambos sistemas
**********************************************

**Ruta saliente en OMniLeads**

Podemos simplemente cambiar en la ruta ya generada el troncal por donde salir a la PSTN, utilizando el troncal generado en este laboratorio:

.. image:: ../img/pbx-outr-oml2pbx.png

Por lo tanto cualquier número que se disque será encaminado hacia el PBX, dejando la decisión de enrutar hacia la PSTN o hacia algún recurso local las llamadas recibidas
desde OMniLeads.


**Encaminar llamadas desde el PBX hacia OMniLeads**

Para poder marcar desde la PBX hacia destinos de OMniLeads es necesario relacionar cada *Ruta entrante* de OMniLeads con una *custom extension*.

Vamos a generar una extensión para la *Campaña Entrante A* que generamos unos laboratorios atrás.
Para ello creamos una ruta entrante en OMniLeads que apunte a dicha campaña:

.. image:: ../img/pbx-inr-from-pbx.png

... y como contraparte la extensión en el PBX:

.. image:: ../img/pbx-custom-exten-pbx-1.png

.. image:: ../img/pbx-custom-exten-pbx-2.png

Nuevamente, los parámetros indicados en las figuras deben ser considentes con los de su configuración.

Prueba de llamadas en ambos sentidos
************************************

* Registrar un softphone como una extensión SIP en el PBX, por otro lado ingresar a OMniLeads con un Agente asignado a la campaña entrante en cuestión.
Luego marcar desde el softphone el número de extensión que mapea ruta entrante de OMniLeads y la llamada deberá ingresar al agente de OMniLeads.

* Desde el agente, realizar una llamada "por fuera de campaña" hacia el número de Extensión asignado al Softphone y comprobar que la llamada haga ring en el softphone.
