.. OML Advance Training documentation master file, created by
   sphinx-quickstart on Thu Mar  5 17:55:47 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Laboratorios OML Advanced Training
==================================

.. toctree::
   :maxdepth: 2
   :caption: Laboratorios:

   omLab-deploy.rst
   omLab-basic-configuration.rst
   omLab-generacion-pstn-emulator.rst
   omLab-pstn.rst
   omLab-intro2campaigns.rst
   omLab-preview-camps.rst
   omLab-manual-camps.rst
   omLab-dialer-camps.rst
   omLab-inbound-calls.rst
   omLab-customer-identification.rst
   omLab-pbx-integration.rst
   omLab-oml2crm.rst
   omLab-IT-manager.rst
   omLab-Addons.rst
