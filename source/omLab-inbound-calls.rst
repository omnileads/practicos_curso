******************
Llamadas Entrantes
******************

En este laboratorio vamos trabajar con el procesamiento de las llamadas entrantes. Al finalizar el capítulo deberíamos conseguir que nuestro OMniLeads implemente el siguiente esquema:


.. image:: ../img/lab-incalls-arq.png
*Figura: esquema implementado al finalizar este laboratorio*


La siguiente sección de la `Documentación Oficial <https://documentacion-omnileads.readthedocs.io/es/develop/campaigns.html#campanas-entrantes>`_ cubre todos estos aspectos.


Creación de campañas entrantes
******************************

Crear dos campañas entrantes con un agente asignado a cada una. En las mismas podrá utilizar la MOH cargada durante anteriores laboratorios.

Enrutamiento
************

Generar una ruta entrante para el *DID: 40404040* cuyo destino sea nuestra cualquiera de las campañas entrantes.

Luego toma un softphone *subscriber* y ejecutar una llamada al número: *40404040*. Si la configuración es correcta, la llamada debería ingresar sobre la campaña asociada a la
ruta entrante.

IVR
***

Crear un IVR utilizando nuestro *audio* facilitado. El menú deberá presentar para el *DTMF 1* que la llamada se encamine hacia la Campaña A, y para el *DTMF 2* hacia la Campaña B.

Generar una nueva ruta entrante para el *DID 40404041* y encaminar la llamada hacia el recientemente creado IVR.
Para corroborar, tomar el softphone *subscriber* y llamar al número 40404041.

Validación condicionada por calendario
**************************************

Generar un grupo horario *lun-vie 09:00 a 18:00*.

A continuación crear un condicional de tiempo asociado al grupo horario anterior, que en caso de coincidir dentro del rango horario del grupo la llamada se encamina
hacia nuestro IVR mientras que en caso de caer fuera del rango horario del grupo, la llamada se envíe hacia un Hangup.

Generar una nueva ruta entrante para el *DID 40404042* y encaminar la llamada hacia el recientemente Condicional de Tiempo creado.
Para corroborar, tomar el softphone *subscriber* y llamar al número 40404042.


Custom destinations
*******************

En este ítem vamos a generar un destino personalizado.

.. image:: ../img/lab-incalls-custom-dst.png

Una vez generado el *custom destionation* en OMniLeads, procedemos a generar el dialplan en el archivo pertinente para ello, es decir: **oml_extensions_custom.conf**

.. code-block:: bash

 [oml_course]
 exten => s,1,Verbose(2, custom dst oml course)
 same => n,Playback(tt-weasels)
 same => n,Hangup()

Luego hacer un "dialplan reload" en el CLI de Asterisk.

Finalmente, podemos añadir un nuevo *DTMF* dentro del IVR existente que invoque al destino personalizado.

.. image:: ../img/lab-incalls-custom-dst-2.png

Por último generar un llamado que caiga sobre el IVR e ingresar la opción que nos lleve al destino personalizado.
