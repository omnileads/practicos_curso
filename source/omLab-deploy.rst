*******************
Deploy de OMniLeads
*******************

En este laboratorio vamos a desplegar la Aplicación en sus diferentes posibilidades.

Pondremos énfasis en el despliegue ***Classic CentOS OML*** para estandarizar todo el trabajo de configuración que se realizará en los próximos laboratorios. De todas maneras
también se cubren los deploys ***Docker prodenv*** y ***Docker Devenv***.

Con respecto al tipo de deploy, desde OMniLeads se alienta el uso del método *Deployer & Nodes*, lo cual permite gestionar múltiples instancias desde el repositorio clonado
sobre la estación de trabajo del Admin.

La siguiente sección de la `Documentación Oficial <https://documentacion-omnileads.readthedocs.io/es/develop/install.html>`_ cubre todos estos aspectos.

Deploy Classic CentOS OML (AIO)
*******************************

A partir de contar con una VM (10 GB de disco + 2 GB de RAM) con CentOS 7 (edición minimal) instalado y con acceso a internet SIN restricciones, llevar a cabo el deploy de la Aplicación,
siguiendo el método (Self-Hosted ó Deployer & Nodes) que el alumno prefiera. Tener bien en cuenta que vamos a desplegar la rama "develop" de OMniLeads.

.. important::

 .. code-block:: bash

   git clone https://gitlab.com/omnileads/ominicontacto.git
   cd ominicontacto
   git checkout develop

Observar el **git checkout develop** que difiere del **git checkout master** indicado en la documentación.

Una vez finalizado el deploy, proceda a ingresar a su instancia utilizando la URL https://X.X.X.X (IP del CentOS Host). Luego seguir lo pasos indicados en la Documentación
`primer acceso a OMniLeads <https://documentacion-omnileads.readthedocs.io/es/develop/install_first_access.html>`_.

.. important::

 No olvide tomar un snapshot de la VM de manera tal que nos quede disponible una versión virgen de OMniLeads para en los últimos capítulos trabajar con *backup & restore*

Deploy Docker prodenv
*********************

A partir de contar con una VM (16 GB de disco + 2 GB de RAM) con Issabel-PBX instalado y con acceso a internet SIN restricciones, llevar a cabo el deploy de la Aplicación,
siguiendo el método (Self-Hosted ó Deployer & Nodes) que el alumno prefiera.

Una vez finalizado el deploy, proceda a ingresar a su instancia utilizando la URL https://X.X.X.X:444 (IP del centos).Luego seguir lo pasos indicados en la Documentación
`primer acceso a OMniLeads <https://documentacion-omnileads.readthedocs.io/es/develop/install_first_access.html>`_.

Deploy Devenv
**************

En este punto es requisito excluyente contar con una estación de trabajo basada en Debian 10, Ubuntu 18.04 (o posteriores).
Si el alumno utiliza cualquier otro sistema operativo, puede ignorar este ítem de la práctica.
