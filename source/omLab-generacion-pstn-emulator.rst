.. _omLab-generacion-pstn-emulator:

******************************
Generación de banco de testing
******************************

Para poder ejecutar todos los laboratorios y/o testear de aquí hacia el futuro cualquier funcionalidad de la plataforma, dejamos en esta sección los pasos para
implementar una instancia de Asterisk configurada como PSTN-Emulator o banco de pruebas.

Con los fines de generar el escenario *OMniLeads -- SIP Trunk -- ITSP (telco) -- Subscribers*

.. image:: ../img/pstn-emulator.png

*Figura: banco de pruebas*


Generar VM con Asterisk
***********************

A partir de contar con una VM corriendo Ubuntu-Server o Debian, simplemente ejecutar el siguiente comando para instalar Asterisk.

.. code-block:: bash

 sudo apt install asterisk

.. Note::
 Es crucial que nuestra VM con Asterisk esté en el mismo segmento de red que la instancia de OMniLeads.


A partir de contar con Asterisk instalado vamos a trabajar con los archivos *sip.conf* y *extensions.conf*

Para el archivo sip.conf, copiar las siguientes lineas al final del mismo.

.. code-block:: bash

 [subscribers](!)
 type=friend
 host=dynamic
 dtmfmode=rfc2833
 secret=omnileads
 context=omnileads
 qualify=yes

 [72007200](subscribers)
 [22002200](subscribers)

 [40404040]
 type=friend
 host=dynamic
 dtmfmode=rfc2833
 secret=omnileads
 context=omnileads
 qualify=yes

Para el archivo extensions.conf se deberán añadir las siguientes lineas al final.

.. code-block:: bash

 [omnileads]
 exten => _[2,7][2,7]00[2,7][2,7]00,1,Verbose(2, from OML to test-phones)
 same => n,Dial(SIP/${EXTEN})
 same => n,Hangup()

 exten => _4040404X,1,Verbose(2, from test-phones to OML)
 same => n,Set(CALLERID(NUM)=${RAND(4140000,5140001)})
 same => n,Dial(SIP/40404040/${EXTEN})
 same => n,Hangup()

 exten => _X.,1,Verbose(2, from OML to test)
 same => n,Set(QUITAR=$[${LEN(${EXTEN})}-1])
 same => n,GotoIf($["${EXTEN:${QUITAR}}" == "6"]?busy)
 same => n,GotoIf($["${EXTEN:${QUITAR}}" == "7"]?completeOutnum)
 same => n,GotoIf($["${EXTEN:${QUITAR}}" == "8"]?noAns)
 same => n,GotoIf($["${EXTEN:${QUITAR}}" == "9"]?congestion)
 same => n,Wait(5)
 same => n,Answer()
 same => n,Playback(demo-congrats)
 same => n,Playback(demo-instruct)
 same => n,Playback(tt-weasels)
 same => n,Playback(demo-congrats)
 same => n,Playback(demo-instruct)
 same => n,Playback(tt-weasels)
 same => n,Hangup()
 same => n(busy),Busy(5)
 same => n,Hangup()
 same => n(completeOutnum),Wait(5)
 same => n,Answer()
 same => n,Playback(tt-weasels)
 same => n,Playback(tt-weasels)
 same => n,Hangup()
 same => n(noAns),Wait(120)
 same => n,Hangup()
 same => n(congestion),Congestion()
 same => n,Hangup()

El dialplan aplicado se encarga de atender cualquier número marcado y reproducir una grabación, la idea es emular una llamada.

Números excepcionales:

* 40404040 al 40404049: si se marca cualquier número dentro de ese rango, se enviarán llamadas hacia OMniLeads.
* 72007200: se envía la llamada a la cuenta 72007200.
* 22002200: se envía la llamada a la cuenta 22002200.
* Los números terminados en "6", darán tono de ocupado.
* Los números terminados en "7", serán atendidos, se pasan dos grabaciones cortas y se termina la llamada.
* Los números terminados en "6", darán tono de ocupado.
* Los números terminados en "8", no atenderán su llamada.
* Los números terminados en "9", darán congestion.

Bajo la premisa de emular la PSTN, también se buscó reproducir llamadas ocupadas, que no contesten o den congestion.

Finalmente ejecutar un *module reload* sobre el CLI de Asterisk para que se implemente nuestra configuración.


Registrar los softphones de prueba
**********************************

En este punto se procede con la comprobación de la configuración aplicada, para ello vamos a registrar como suscritores a las cuentas SIP recientemente creadas.
Estas son: 72007200 y 22002200.

Para ello podemos utilizar cualquier Softphone que corra sobre su estación de trabajo o smartphone.

Probar una llamada a cualquier número y comprobar que el Asterisk de prueba atiende la misma y reproduce una grabación.
