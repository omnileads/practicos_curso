.. _omLab-preview-camps:

****************
Campañas preview
****************

La siguiente sección de la `Documentación Oficial <https://documentacion-omnileads.readthedocs.io/es/develop/campaigns_preview.html#about-previewcamp>`_ cubre todos estos aspectos.

* Crear una campaña preview utilizando la base de contactos facilitada, el formulario "encuesta" creado y el lote de calificaciones.
La calificación "Encuesta realizada" deberá desplegar el formulario en cuestión.

* Ejecutar una serie de llamadas, probar diferentes calificaciones.

* Probar desactivar las grabaciones de la campaña y utilizar la grabación bajo demanda.
