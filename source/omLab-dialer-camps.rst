********************************
Campañas con discador predictivo
********************************

En esta sección se pretende poner en marcha las campañas con discador predictivo. Como es sabido, OMniLeads utiliza Wombat Dialer como componente crucial
a la hora de generar las llamadas.

Por lo tanto debemos proceder con la `configuración necesaria <https://documentacion-omnileads.readthedocs.io/es/develop/maintance.html#configuracion-del-modulo-de-discador-predictivo>`_
sobre dicho componente.

Luego se avanza con la configuración de campañas predictivas, utilizando la base de contactos facilitada para que el Dialer efectúe llamadas hacia nuestro PSTN-Emulator.

La siguiente sección de la `Documentación Oficial <https://documentacion-omnileads.readthedocs.io/es/develop/campaigns_dialer.html#about-dialercamp>`_ cubre todos los aspectos relacionados
a campañas de discado predictivo.

**Crear una campaña**

A continuación se pide crear una campaña del tipo predictiva utilizando la base de contactos más extensa y el formulario "Venta". La calificación "Venta" deberá disparar
el formulario pertinente.

**Ejecutar un reciclado**

Una vez finalizado el recorrido de la base sobre la campaña, aplicar un reciclado de campaña.

**Cambiar base de contactos de campaña**

En este punto procederemos con un cambio de base de contactos sobre nuestra campaña existente.

.. important::

  Para llevar a cabo esta tarea es impresindible que el esqueleto de la base de contactos que desea asignar como reemplazo debe ser exactamente igual a la base reemplazada.
  Es decir independientemente de los contactos y la cantidad, las columnas deben ser exactamente iguales.

Aclarado este tema, procedemos con la aplicación de un cambio de base de contactos a nuestra campaña.
