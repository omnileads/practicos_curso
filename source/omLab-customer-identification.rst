************************************************
Identificación de clientes en llamadas entrantes
************************************************

Este módulo nos permite solicitar al llamante que ingrese su código de identificación, por lo tanto vamos a implementar dicha funcionalidad en nuestro
diagrama de llamadas entrantes.

La siguiente sección de la `Documentación Oficial <https://documentacion-omnileads.readthedocs.io/es/develop/campaigns_inbound_customer_id.html#about-customer-id>`_ cubre todos estos aspectos.

Identificación de llamada entrante e interacción con WS (servicio web) tipo 1
*****************************************************************************

En este punto vamos a configurar una interacción con un servicio web al que simplemente pasamos el *ID de cliente* capturado por DTMF y esperamos del servicio
que nos devuelva *true* o *false*. Si bien en este ambiente netamente pedagógico nosotros vamos a generar las respuestas utilizando `la web <https://https://www.mocky.io/>`_
la idea es utilizar esta funcionalidad para por ejemplo consultar al servicio web el estado de la cuenta del cliente que está llamando donde *true* puede significar
que el cliente tiene su pago al día y *false* lo contrario, otra utilización es verificar si es un cliente ocasional o con un plan. En base a ello OMniLeads nos permite encaminar la llamada hacia un lugar u otro.

En primer lugar entonces generamos "el servicio web" utilizando la web mencionada.
Vamos a generar 2 URLs una que devuelva *true* y otra *false*. Volviendo a recordar que esto es netamente una prueba de concepto y funcionalidad en OMniLeads.

**URL que devuelve FALSE**

.. image:: ../img/lab-in-custom-id-url-false-1.png
*Figura: generacion de URL que devuelve FALSE*

Al abrir el URL con el browser deberíamos observar la respuesta JSON generada.

.. image:: ../img/lab-in-custom-id-url-false-2.png
*Figura: generacion de URL que devuelve FALSE*

Por lo tanto GUARDAMOS esta URL-False porque la vamos a usar en los siguientes pasos.

**URL que devuelve TRUE**

.. image:: ../img/lab-in-custom-id-url-true-1.png
*Figura: generacion de URL que devuelve TRUE*

Al abrir el URL con el browser deberíamos observar la respuesta JSON generada.

.. image:: ../img/lab-in-custom-id-url-true-2.png
*Figura: generacion de URL que devuelve TRUE*

Por lo tanto GUARDAMOS esta URL-True porque la vamos a usar en los siguientes pasos.

A continuación debemos crear un objeto identificación de llamadas entrantes en OMniLeads:

.. image:: ../img/lab-in-custom-id-oml.png
*Figura: generacion del objeto identificación de llamadas*

En la figura se remarca el campo en donde debemos insertar cada URL generada en los pasos anteriores.
Podemos elegir hacia donde encaminar las llamadas si el "Servicio Web" devuelve *True* y hacia donde si es *False* la respuesta del JSON recibido.

Para poder derivar llamadas hacia la funcionalidad podemos generar una nueva ruta entrante:

.. image:: ../img/lab-in-custom-id-inr.png
*Figura: ruta entrante que deriva al objeto identificación de llamadas*

Lo que finalmente nos queda por probar es:

* 1 - Utilizando el URL-False generar una llamada entrante al DID de nuestra ruta y comprobar que se envíe la llamada hacia el destino False.
* 2 - Utilizando el URL-True generar una llamada entrante al DID de nuestra ruta y comprobar que se envíe la llamada hacia el destino True.

Obviamente en un caso real no habrá que andar cambiando de URLs, simplemente una misma URL devolverá un JSON true o false basado en el ID que nosotros enviamos.


Identificación de llamada entrante e interacción con WS (servicio web) tipo 2
*****************************************************************************

En este punto también consultaremos un servicio web, pero utilizando la configuración que permite que el servicio web nos devuelva una tupla (x,y) que indica hacia qué destino de OMniLeads hay que enviar la llamada.

OMniLeads identifica cualquier destino interno usando un par de valores (X = tipo de módulo, Y = objeto particular dentro del tipo de módulo). Por lo tanto el servicio web (por ejemplo un endpoint de la API de un CRM) podría
directamente encaminar las llamadas hacia diferentes campañas entrantes. Una aplicación de esto podría ser encaminar las llamadas dependiendo de si el cliente que nos llamada tiene planes con mayor nivel de servicio, por lo
tanto desde el CRM decidir sobre a que campaña enviar las llamadas de los clientes.

Como nosotros tenemos dos campañas entrantes generadas, vamos a utilizar nuevamente `esta web <https://https://www.mocky.io/>`_ para generar otros JSON funcionales a esta práctica.

Pero antes vamos a averiguar los *ID de campaña* de cada una de las campañas entrantes, para ello simplemente ingresar a ellas y revisar el URL en el browser.

.. image:: ../img/lab-in-custom-id-url-idcamp.png

Hacemos lo mismo con la otra campaña.

Ahora vamos a generar una URL similar a la siguiente figura:

.. image:: ../img/lab-in-custom-id-url-dst.png
*Figura: generacion de URL que devuelve el destino concreto a donde encaminar las llamadas*

.. image:: ../img/lab-in-custom-id-url-dst-1.png

Como podemos ver la tupla que configuramos en el URL es (1,Y) donde 1=campaña entrante e *Y* es el id de la campaña en cuestión.

Ahora vamos a utilizar la URL recientemente generada para configurar un nuevo elemento *identificación de cliente* en OML como indica la siguiente figura:

.. image:: ../img/lab-in-custom-id-url-dst-oml.png

Como muestra la figura, además de ingresar la nueva URL del servicio web, debemos modificar el campo *tipo de interacción* con el valor que implementa la interacción del tipo enrutamiento desde el servicio web.

Ahora podemos generar una nueva ruta entrante que apunte a este nuevo objeto y probar con una llamada. Si todo fue bien configurado entonces la llamada será enviada a la campaña cuyo ID se devuelve en el JSON.
